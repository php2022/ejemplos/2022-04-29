<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        /**
         * Funcion que recibe un array de numeros
         * y retorna el mayor
         * @param array $numeros numeros a evaluar
         * @return int numero mayor
         */
        function uno($numeros){
            //utilizo la funcion max de php
            $numero=max($numeros);
            return $numero;
        }
        
        echo uno([1,2,6,9,23,1,2,3]);
        
        
        
        function unoMia($numeros=[0]){//argumento valor predeterminado
            $numero=$numeros[0];
            
            //recorro el array y me quedo con el numero mayor
            foreach($numeros as $valor){
                if($numero<$valor){
                    $numero=$valor;
                }
            }
            
            return $numero;
        }
        
        echo unoMia([2,3,4,5,12,1,2,3]);
        
        
        /**
         * Funcion que retorna un array ordenado
         * de forma descendente
         * @param array &$numeros vector de numeros
         * 
         */
        function dos(&$numeros){
            rsort($numeros);
        }
        
        $vector=[2,3,4,5,23,56,1];
        $vectorOrdenado=dos($vector);
        var_dump($vector);//el vector original ordenado
        var_dump($vectorOrdenado);//null
        
        
        function dosMiaIzquierda(&$numeros){
            do{
                $ordenado=true;
                for($indice=0;$indice<count($numeros)-1;$indice++){
                    if($numeros[$indice]<$numeros[$indice+1]){
                        $auxiliar=$numeros[$indice];
                        $numeros[$indice]=$numeros[$indice+1];
                        $numeros[$indice+1]=$auxiliar;
                        $ordenado=false;
                    }
                }
            }while($ordenado==false);
            
        }
        $n1=[1,2,3,4,5,11,2,333,2,1,23,4];
        dosMiaIzquierda($n1);
        var_dump($n1);
        
        
        function dosMiaDerecha(&$numeros){
            for($indice=0;$indice<count($numeros)-1;$indice++){
                for($k=$indice+1;$k<count($numeros);$k++){
                    if($numeros[$indice]<$numeros[$k]){
                        $aux=$numeros[$indice];
                        $numeros[$indice]=$numeros[$k];
                        $numeros[$k]=$aux;
                    }
                }
            }
        }
        $n2=[1,2,3,4,5,11,2,333,2,1,23,4];
        dosMiaIzquierda($n2);
        var_dump($n2);
        
        ?>
    </body>
</html>
