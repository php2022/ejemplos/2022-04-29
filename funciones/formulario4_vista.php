<form>
    <div>
        <label for="radio">Radio</label>
        <input type="number" id="radio" name="radio" value="<?= $radio ?>">
    </div>
    <div>
        <label for="area">Área</label>
        <input type="number" id="area" name="area" readonly="true" value="<?= $area ?>">
    </div>
    <div>
        <button name="calcular">Calcular</button>
    </div>
</form>
<svg width="800" height="800"> 
    <?= circulo($radio*4) ?>
</svg>
