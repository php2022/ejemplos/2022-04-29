<?php
    function calcular($numero1,$numero2,&$resultado){//con &pasamos por referencia
        $resultado["cociente"]=(int)($numero1/$numero2);
        $resultado["resto"]=$numero1%$numero2;
    }
    
    function render($numero1,$numero2,$resultado){
        extract($resultado);
        include "formulario3.php";
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //inicializacion de variables
        $numero1=0;
        $numero2=0;
        $resultado=[
            "cociente"=>0,
            "resto"=>0
        ];
        //comprobar si he pulsado el boton de enviar
        //if($_GET){
        if(isset($_GET["calcular"])){//compruebo la existencia del indice
            $numero1=$_GET["numero1"];
            $numero2=$_GET["numero2"];
            
            calcular($numero1,$numero2,$resultado);   
        }
        //mostrar el formulario
        render($numero1,$numero2,$resultado);
        
        ?>
    </body>
</html>
