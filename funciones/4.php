<?php
    function calcular($radio,&$area){//con &pasamos por referencia
        $area=pi()*pow($radio,2);
    }
    
    function render($radio,$area){
        include "formulario4_vista.php";
    }
    
    function circulo($radio){
        return '<circle cx="100" cy="100" r="'.$radio.'" stroke="purple" stroke-width="4" fill="purple"/>';
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //inicializacion de variables
        $radio=0;
        $area=0;
        
        //comprobar si he pulsado el boton de enviar
        //if($_GET){
        if(isset($_GET["calcular"])){//compruebo la existencia del indice
            $radio=$_GET["radio"];
            
            calcular($radio,$area);   
        }
        //mostrar el formulario
        render($radio,$area);
        ?>
    </body>
</html>
