<form>
    <div>
        <label for="numero1">Numero 1</label>
        <input type="number" id="numero1" name="numero1" value="<?= $numero1 ?>">
    </div>
    <div>
        <label for="numero2">Numero 2</label>
        <input type="number" id="numero2" name="numero2" value="<?= $numero2 ?>">
    </div>
    <div>
        <label for="cociente">Cociente</label>
        <input type="number" id="cociente" name="cociente" value="<?= $cociente ?>">
    </div>
    <div>
        <label for="resto">Resto</label>
        <input type="number" id="resto" name="resto" value="<?= $resto ?>">
    </div>
    <div>
        <button name="calcular">Enviar</button>
    </div>
</form>
