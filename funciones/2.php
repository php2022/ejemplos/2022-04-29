<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        /**
         * Funcion que recibe dos numeros y en el
         * tercer argumento coloca el resultado
         * @param type $n1
         * @param type $n2
         * @param type $r
         */
        function sumar($n1,$n2,&$r){//$r pasa referencia
            $r=$n1+$n2;
        }
        $numero1=1;
        $numero2=3;
        $resultado=0;
        
        sumar($numero1,$numero2,$resultado);
        echo $resultado;
        
        ?>
    </body>
</html>
