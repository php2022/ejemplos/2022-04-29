<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        function dibujar($texto){
            echo "<div style=\"margin:10px; background-color:#CCC; width:100px; height:100px\">$texto</div>";
        }
        dibujar("Hola clase");
        
        
        function dibujar1($texto){
        ?>
            <div style="margin:10px; background-color:#CCC; width:100px; height:100px"><?= $texto ?></div>
        <?php
        }
        dibujar1("hola clase");
        
        function dibujar2($texto){
            $caja= "<div style=\"margin:10px; background-color:#CCC; width:100px; height:100px\">$texto</div>";
            echo $caja;
        }
        dibujar2("Hola clase");
        
       
        function dibujar3($texto){
            $caja= file_get_contents("caja.plantilla");
            //sustituir {{texto}} por $texto
            $final=str_replace("{{texto}}",$texto,$caja);
            var_dump($caja);
            var_dump($final);
        }
        
        dibujar3("Hola clase");
        
        
        function dibujar4($texto){
            include "caja.inc";
        }
        
        dibujar4("Hola clase");
        
        
        ?>
    </body>
</html>
