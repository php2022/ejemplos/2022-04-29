<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $a="Ejemplo de clase";
        
        //sustituir la o por un -
        $final=str_replace(
                "o",//caracter a sustituir
                "-",//caracter de reemplazo
                $a//el texto donde sustituye los caracteres
                );
        echo "$final<br>";//la salida es "Ejempl- de clase"
        
        //sustituir todas las e por -
        $final=str_ireplace(//ireplace reemplaza mayusculas y minusculas
                "e",//caracter a sustituir
                "-",//caracter de reemplazo
                $a//el texto donde sustituye los caracteres
                );
        echo "$final<br>";//la salida es "-j-mplo d- clas-"
        
        //sustituir clase por aula
        $final=str_replace(
                "clase",//caracter a sustituir
                "aula",//caracter de reemplazo
                $a//el texto donde sustituye los caracteres
                );
        echo "$final<br>";//la salida es "Ejemplo de aula"
        
        ?>
    </body>
</html>
